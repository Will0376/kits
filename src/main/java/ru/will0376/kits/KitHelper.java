package ru.will0376.kits;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.text.TextComponentString;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class KitHelper {
	public static void setKit(EntityPlayer player, String name, long time) throws IOException {
		NBTTagCompound nbt = new NBTTagCompound();
		NBTTagList kit = NBTHelper.getAllItems(player.inventory.mainInventory);
		nbt.setTag("kit", kit);
		nbt.setLong("time", time);
		NBTHelper.saveNBTToFile(nbt, new File(Kits.kitFolder, name + ".nbt"));
	}

	public static void setTime(EntityPlayer p, String name, long time) throws IOException {
		File file = new File(Kits.kitFolder, name.toLowerCase() + ".nbt");
		if (!file.exists()) {
			p.sendMessage(new TextComponentString("§4Такого набора нет!"));
			p.playSound(SoundEvents.ENTITY_GENERIC_EXPLODE, 1, 2);

			return;
		}
		NBTTagCompound nbt = NBTHelper.readNBTFromFile(file);
		nbt.setLong("time", time);
		NBTHelper.saveNBTToFile(nbt, file);
		p.sendMessage(new TextComponentString("§2Время изменено."));
		p.playSound(SoundEvents.BLOCK_ANVIL_BREAK, 1, 1);
	}

	public static void delKit(String name, EntityPlayer p) {
		File k = new File(Kits.kitFolder, name.toLowerCase() + ".nbt");
		if (k.exists()) {
			k.delete();
			p.sendMessage(new TextComponentString("§2Набор удален."));
			p.playSound(SoundEvents.BLOCK_ANVIL_BREAK, 1, 1);
		} else {
			p.sendMessage(new TextComponentString("§4Такого набора нет!"));
			p.playSound(SoundEvents.ENTITY_GENERIC_EXPLODE, 1, 0);
		}
	}

	public static void getKit(EntityPlayer p, String name, boolean b) throws IOException {
		File file = new File(Kits.kitFolder, name.toLowerCase() + ".nbt");
		if (!file.exists()) {
			p.sendMessage(new TextComponentString("§4Такого набора нет!"));
			p.playSound(SoundEvents.ENTITY_GENERIC_EXPLODE, 1, 2);
			return;
		}
		NBTTagCompound nbt = NBTHelper.readNBTFromFile(file);
		long time = nbt.getLong("time");
		NBTTagList Inv = nbt.getTagList("kit", 10);

		if (!b) {
			if (new File(Kits.playerFolder, name.toLowerCase() + "-" + p.getName()).exists()) {
				NBTTagCompound temp = NBTHelper.readNBTFromFile(new File(Kits.playerFolder, name.toLowerCase() + "-" + p.getName()));
				long tpl = temp.getLong("time");
				long calc = (System.currentTimeMillis() - tpl) / 1000;
				Time t = new Time(time - calc);
				if (calc < time) {
					p.sendMessage(new TextComponentString("§4Вы не можете получить этот набор, раньше чем через §c" + t.getFormat()));
					p.playSound(SoundEvents.BLOCK_ANVIL_BREAK, 1, 0);
					return;
				}
			}
		}
		NBTHelper.addListToPlayer(Inv, p);
		p.sendMessage(new TextComponentString("§6Получен набор §c" + name.toLowerCase()));
		p.playSound(SoundEvents.BLOCK_ANVIL_LAND, 1, 0);

		NBTTagCompound temp = new NBTTagCompound();
		temp.setLong("time", System.currentTimeMillis());
		NBTHelper.saveNBTToFile(temp, new File(Kits.playerFolder, name.toLowerCase() + "-" + p.getName()));
	}

	public static List<String> getLogs() {
		List<String> l = new ArrayList<>();
		for (File file : Objects.requireNonNull(Kits.kitFolder.listFiles())) {
			String kit = file.toString().substring(file.toString().lastIndexOf(File.separator) + 1);
			if (kit.contains(".nbt")) {
				l.add(kit.replace(".nbt", ""));
			}
		}
		return l;
	}
}
