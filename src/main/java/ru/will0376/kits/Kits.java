package ru.will0376.kits;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;

import java.io.File;

@Mod(modid = Kits.MOD_ID, name = Kits.MOD_NAME, version = Kits.VERSION, acceptedMinecraftVersions = "[1.12.2]", acceptableRemoteVersions = "*")
public class Kits {

	public static final String MOD_ID = "kits";
	public static final String MOD_NAME = "Kits";
	public static final String VERSION = "1.0";
	public static File cfgFolder;
	public static File kitFolder;
	public static File playerFolder;
	@Mod.Instance(MOD_ID)
	public static Kits INSTANCE;

	@Mod.EventHandler
	public void preinit(FMLPreInitializationEvent event) {
		cfgFolder = new File(event.getModConfigurationDirectory(), MOD_NAME);
		cfgFolder.mkdir();
		kitFolder = new File(cfgFolder, "kits");
		kitFolder.mkdir();
		playerFolder = new File(cfgFolder, "players");
		playerFolder.mkdir();
	}

	@Mod.EventHandler
	public void event(FMLServerStartingEvent event) {
		event.registerServerCommand(new Command());
	}
}
