package ru.will0376.kits;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

import java.util.ArrayList;

public class Command extends CommandBase {
	@Override
	public String getName() {
		return "kit";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "";
	}

	@Override
	public int getRequiredPermissionLevel() {
		return 0;
	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		return true;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		try {
			switch (args.length) {
				case 3:
					if (sender.canUseCommand(4, "kits.admin") && args[0].matches("^[A-Za-z0-9]{1,10}$") && args[1].matches("^[A-Za-z0-9]{1,10}$") && args[2].matches("^[0-9]{1,32}$")) {
						long time = Long.parseLong(args[2]);
						if (args[0].equalsIgnoreCase("set") && !args[1].equalsIgnoreCase("time")) {
							KitHelper.setKit((EntityPlayer) sender, args[1], time);
							sender.sendMessage(new TextComponentString("Done!"));
							return;
						}
						if (args[0].equalsIgnoreCase("time")) {
							KitHelper.setTime((EntityPlayer) sender, args[1], time);
							sender.sendMessage(new TextComponentString("Done!"));
							return;
						}
					}
					break;
				case 2:
					if (sender.canUseCommand(4, "kits.admin") && args[0].matches("^[A-Za-z0-9]{1,10}$") && args[1].matches("^[A-Za-z0-9_]{1,10}$")) {
						if (args[0].equalsIgnoreCase("set") && !args[1].equalsIgnoreCase("time")) {
							KitHelper.setKit((EntityPlayer) sender, args[1], 86400);
							return;
						}
						if (args[0].equalsIgnoreCase("del")) {
							KitHelper.delKit(args[1], (EntityPlayer) sender);
							return;
						}
						EntityPlayer pl = server.getPlayerList().getPlayerByUsername(args[1]);
						if (pl == null) {
							sender.sendMessage(new TextComponentString("§4Игрок не найден"));
							((EntityPlayer) sender).playSound(SoundEvents.ENTITY_GENERIC_EXPLODE, 1, 2);
							return;
						}
						KitHelper.getKit(pl, args[0], true);
						return;
					}
					break;
				case 1:
					if (sender.canUseCommand(4, "kits.kit." + args[0]) && args[0].matches("^[A-Za-z0-9]{1,10}$")) {
						KitHelper.getKit((EntityPlayer) sender, args[0], sender.canUseCommand(4, "kits.bypass"));
						return;
					} else {
						sender.sendMessage(new TextComponentString("§4У вас нет разрешения на этот набор!"));
						((EntityPlayer) sender).playSound(SoundEvents.ENTITY_GENERIC_EXPLODE, 1, 1);
					}
					break;
				case 0:
					ArrayList<String> l = new ArrayList<>();
					for (String s : KitHelper.getLogs()) {
						if (sender.canUseCommand(4, "kits.kit." + s)) {
							l.add(s);
						}
					}
					sender.sendMessage(new TextComponentString("§6Наборы: §f" + l.toString().substring(1, l.toString().length() - 1).replace(",", "")));
					((EntityPlayer) sender).playSound(SoundEvents.BLOCK_CHEST_OPEN, 1, 1);
					break;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
