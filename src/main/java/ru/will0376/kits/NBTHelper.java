package ru.will0376.kits;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class NBTHelper {
	public static void saveNBTToFile(NBTTagCompound nbt, File file) throws IOException {
		FileOutputStream fileoutputstream = new FileOutputStream(file);
		CompressedStreamTools.writeCompressed(nbt, fileoutputstream);
		fileoutputstream.close();
	}

	public static NBTTagCompound readNBTFromFile(File file) throws IOException {
		FileInputStream fileInputStream = new FileInputStream(file);
		NBTTagCompound nbtTagCompound = CompressedStreamTools.readCompressed(fileInputStream);
		fileInputStream.close();
		return nbtTagCompound;
	}

	public static NBTTagList getAllItems(NonNullList<ItemStack> list) {
		NBTTagList ret = new NBTTagList();
		list.forEach(e -> {
			if (!e.isEmpty()) {
				NBTTagCompound nbt = new NBTTagCompound();
				e.writeToNBT(nbt);
				ret.appendTag(nbt);
			}
		});
		return ret;
	}

	public static void addListToPlayer(NBTTagList list, EntityPlayer player) {
		World world = player.world;
		list.forEach(nbtBase -> {
			ItemStack stack = new ItemStack((NBTTagCompound) nbtBase);
			if (!player.addItemStackToInventory(stack)) {
				world.spawnEntity(new EntityItem(world, player.posX, player.posY + 1, player.posZ, stack));
			}
		});
	}
}
